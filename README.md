# Дипломная работа


# Myblog_app

Данный проект является выпускной дипломной работой по курсу DevOps компании Accenture  

## Автор  

<img src="image_for_diplom/ava.png" width="200">  
Владимиров Александр  

## stage 1
Ранеры были запущены в докере, листинг команд по созданию и регистрации ранеров в  listing/runners.  
В качестве дефолтного образа выступает image dind, а executor docker.

<img src="image_for_diplom/runners.PNG">  
<img src="image_for_diplom/reg_runner.PNG">  

Сам пайплан выглядит следующим образом:  
<img src="image_for_diplom/pipeline.PNG">  
1. В stage linter происходит проверка кода линтами, это делается перед сборкой для того, чтобы не создавать образ с "плохим кодом"  
2. В stage build происходит билд проекта и пуш в GitLab Container Registry, данный stage состоит из 2 job'ов тк происходит сборка Python приложения и фронтенд части(фронтенд часть билдится и результат сборки копируется в образ nginx)  
3. Stage deploy происходит при помощи ансибла, в зависимости от ветки запускает template c защищёнными переменым проекта  
<img src="image_for_diplom/protected_varibe.PNG">  

А именно по таскам/ролям:
- * первая таска в плейбуке служит для того, чтобы ssh-ключ, который находится в проекте в зашифрованном виде в групповых переменных, установить на машине для дальнешего продключения к удалённым хостам 
- * Следом идёт подключение к удалённому хосту, значение которого зависит от ветки и устанавливаем рабочие зеркала  
- * Следующая роль устанавливает docker и docker-compose на удалённом хосте  
- * Роль generate_ssl производит генерацию сертификатов в нужные директории, которые при деплое будут прокинуты волумом к nginx, а так же преобразует docker-compose.yml.j2 template на целевой хост в docker-compose.yml
- * Роль deploy_app сначала проверяет есть ли живые контейнеры, если есть, то убивает и производит деплой приложения про помощи модуля ансибл communitydocker.docker_compose  

4. Так же как и stage деплоя, stage testing использует template и скрытые переменные. На этом стейдже происходит подняте образа с jmeter и нагрузочное тестирование на API. Отчёт по тестированию сохраняется в артефакты  
Для использования защищённых переменных ветки dev и prod были защищены и изменения туда могут поступать от пуша пользователей 
maintainers и выше или merge requests

Работающие контейнеры на prod
<img src="image_for_diplom/prod_run_container.PNG">
Работающие контейнеры на dev 
<img src="image_for_diplom/dev_run_container.PNG"> 
## stage 2

Конфигурационные файлы для всего ELK стека находятся в  listing/elk
На машине Agregator был развернут Logstash, который слушает 5044 порт, забирая оттуда логи, которые Beats туда отправляет. Logstash так же занимается дополнительной обработкой логов, разрезанием message на более мелкие поля при помощи фильтров, и в зависимости от содержания полей отправляет в соответсвующие индексы.  
Для сбора были выбранны:

* syslogs, которые собирает внутренный модуль Beats
* auth.log, которые собирает внутренный модуль Beats
* специальный input Docker, который позволяет собрать все логи о Docker контейнерах
* input из файла для передачи логов приложения  
В индексе docker находятся докер логи, фильтрация для отправления происходит по полю тег  
<img src="image_for_diplom/docker_logs.PNG">  
В индексе authlogs находятся логи, фильтрация для отправления происходит по полю event.dataset   
<img src="image_for_diplom/authlogs.PNG">  
В индексе syslogs находятся логи, фильтрация для отправления происходит по полю event.dataset   
<img src="image_for_diplom/syslogs.PNG">  
В индексе application находятся логи, которые не прошли фильтрацию в другие индексы   
<img src="image_for_diplom/app_logs.PNG"> 

### Получившийся dashboard  
<img src="image_for_diplom/all_dashboard.PNG">  
Для настройки первого дашборда были использованы поля host.name.keyword и log.file.path.keyword
Для путей контейнера, где находятся логи, используется уникальный хеш, по нему мы можем определить количество созданых контейнеров за определённое время.  
<img src="image_for_diplom/count_conatianer_settings1.PNG">  
<img src="image_for_diplom/count_conatianer_settings2.PNG">  
Для настройки 2 дашборда были использованы поля system.auth.hostname.keyword и system.auth.user
 <img src="image_for_diplom/count_connect_vladimirov_settings.PNG">    
 <img src="image_for_diplom/count_connect_vladimirov_settings2.PNG">  
system.auth.user испоьзуется в качестве фильтра с поиском значения vladimirov, по заданию указано ubuntu и рут, но на данные машины с такими пользователями не подключались, поэтому на 3 борде значения = 0

## Stage 3
Конфигурационные файлы для развёртки node-экспортёров, Prometheus, Alertmanager, Telegrambot находятся listing/prom_graf Конфигурационные файлы postgre-expoter, nginx-exporter, cadvisior находятся в  ansible/roles/generate_ssl/templates/docker-compose.yml.j2. Это сделано для подъёма экспортёров с приложением в 1 сети docker-compose.
<img src="image_for_diplom/prom_target.PNG">  
<img src="image_for_diplom/prom_allert.PNG">  
Alertmanager, Telegrambot так же поднимаются при помощи docker-compose
<img src="image_for_diplom/run_alerting_container.PNG">  
<img src="image_for_diplom/reg_bot.PNG">  
<img src="image_for_diplom/telegram_bot_message.PNG">  
Получившиеся в grafana dashboard

1. cadvisior dashboard  
<img src="image_for_diplom/graf_cadvisior.PNG">  
2. node dashboard 
<img src="image_for_diplom/graf_node.PNG">
3. postgre board
<img src="image_for_diplom/graf_postgre.PNG"> 
Сообщения отказа любой части инфраструктуры считаются критичными, не смотря на возможный спам от бота, данные алерты могут показать не только отказ сервисов, но и подозрительные действия в инфраструктуре

## Stage 4
 В данном пункте проходит нагрузочное тестирование с помощью приложения jmeter в non-gui режиме  
 Результаты  нагрузочного тестирования  
 <img src="image_for_diplom/report_test.PNG">  


## Stage 5
Выпоненные запросы на проде: 
<img src="image_for_diplom/post_request_prod.PNG">  
<img src="image_for_diplom/get_request_prod.PNG">   
<img src="image_for_diplom/ssl_prod.PNG">  

Выпоненные запросы на деве: 
<img src="image_for_diplom/post_request_dev.PNG">  
<img src="image_for_diplom/get_request_dev.PNG">   
<img src="image_for_diplom/ssl_dev.PNG">  

## Автор  
Владимиров Александр
<img src="image_for_diplom/ava.png" width="200">  

