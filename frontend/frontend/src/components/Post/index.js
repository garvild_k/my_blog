import React, { useEffect, useState } from 'react';
import AddPost from '../AddPost';
import './Post.css';
import { get_posts } from '../sevices/mediaAPI';

function Task({ task }) {
  return (
    <div className="mb-4">
      <div className="card">
        <div className="card-header">{task.title}</div>
        <div className="card-body">
          <blockquote className="blockquote mb-0">
            <p>{task.text}</p>
          </blockquote>
        </div>
      </div>
    </div>
  );
}
function Post() {
  const [tasks, setTasks] = useState();
  const reload = async () => {
    const posts = await get_posts();
    console.log(posts)
    setTasks(posts.posts );
  };
  useEffect( () => {
    reload()
  }, []);
  
  return (
    <div className="container">
      <div className="header">
        <h1 className="display-1">Мой блог</h1>
      </div>
      <AddPost reload={reload} />
      <div className="tasks">
        {tasks && tasks.map((task, index) => <Task task={task} index={index} key={index} />)}
      </div>
    </div>
  );
}

export default Post;
