import React, { useState } from 'react';
import { add_posts } from '../sevices/mediaAPI';

export default function AddPost({ reload }) {
  const [text, setText] = useState();
  const [title, setTitle] = useState();
  const [error, setError] = useState(false);

  const parseInput = (event) => {
    event.preventDefault();
    if (add_posts(title, text)) {
      reload();
      setError(false);

    } else {
      setError('УУУПСС....Что пошло не так!');
    }
  };

  return (
    <div className="mb-4">
      <div className="mb-3">
        <input
          className="form-control"
          name="title_form"
          onChange={(e) => setTitle(e.target.value)}
          placeholder="введите название поста"></input>
      </div>
      <div className="mb-3">
        <textarea
          className="form-control"
          name="text_form"
          onChange={(e) => setText(e.target.value)}
          rows="5"
          placeholder="введите текст поста"></textarea>
      </div>
      <div className="row align-items-end">
        <div className="col-md-10"></div>
        <div className="col-md-2">
          <input
            className="form-control btn btn-secondary"
            type="submit"
            onClick={parseInput}
            text="Создать пост"></input>
        </div>
      </div>
      {error && <span style={{ color: 'red' }}>{error}</span>}
    </div>
  );
}
