
export async function get_posts() {
  let data;
  try {
    const response = await fetch(window.origin+"/api/get_post", {
      method: 'GET',
      mode:"cors",
      headers: {
        'Content-Type': 'application/json',
      }});
    if (response.ok) {
      data = await response.json();
    } else {
      data = null;
    }
  } catch (e) {
    data = null;
  }
  
  return await data;
}

export async function add_posts(title, text) {
  try {
    const response = await fetch(window.origin+"/api/add_post", {
      method: 'POST',
      mode:"cors",
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ title, text }),
      // body data type must match "Content-Type" header
    });
    
   
    return response.ok
  } catch (e) {
    console.log(e)
    return false;
  }
}
