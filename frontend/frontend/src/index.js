import React from 'react';
import ReactDOM from 'react-dom';
import Post from './components/Post';
import 'bootstrap/dist/css/bootstrap.min.css';

ReactDOM.render(<Post />, document.getElementById('root'));
