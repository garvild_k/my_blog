# React-app
# Описание
Данный проект является лаборатоными работами в курсе Devops в блоке Development от Accenture.  

## Составляющие ПО
Для выполнения задания используется Python, Pycharm, Flask, SQLAlchemy, Node.js, React.js
## Запуск
    npm i
    npm start
### Лабораторная 3
Суть 3 лабораторной получить навыки разработки UI, который обращается к API с использованием библотеки React.
## Авторы
<img src="images_for_readme/ava.png" width="200"> Владимиров Александр 
## Полезные ссылки
[Самоучитель по питону](https://pythontutor.ru/)  
