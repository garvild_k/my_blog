import os
from flask import Flask
from flask import (
    request
)
from sqlalchemy import text
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_cors import CORS

import logging

logging.basicConfig(filename='logs/app.log')

app = Flask(__name__)
cors = CORS(app, resources={r"/*": {"origins": r"*"}})

app.config['SQLALCHEMY_DATABASE_URI'] = str(os.environ.get("DB_CONNECTION"))
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)
migrate = Migrate(app, db)


class PostsModel(db.Model):
    __tablename__ = 'posts'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    title = db.Column(db.String())
    text = db.Column(db.String())

    def __init__(self, title, text):
        self.title = title
        self.text = text

    def __repr__(self):
        return f"<Post {self.title, self.text}>"


@app.route('/api/add_post', methods=['POST'])
def add_post():
    data = request.get_json()
    if data is not None:
        new_post = PostsModel(title=data['title'], text=data['text'])
        db.session.add(new_post)
        db.session.commit()
        return {"message": "Post append"}
    else:
        return {"message": "Post error"}



@app.route('/api/get_post', methods=['GET'])
def get_post():
    sql = text('select id, title, text from posts')
    result = db.engine.execute(sql)
    results = [
        {
            "id": post.id,
            "title": post.title,
            "text": post.text
        } for post in result]

    return {"count": len(results), "posts": results, "message": "success"}
