# Python-basics
# Описание
Данный проект является лаборатоными работами в курсе Devops в блоке Development от Accenture.  

## Составляющие ПО
Для выполнения задания используется Python, Pycharm, Flask, SQLAlchemy
## Запуск
    pip install -r requirements.txt  
    запустить скрипт db  для создания базы
    flask db init  
    flask db migrate  
    flask db upgrade
    flask run  
### Лабораторная 2
Суть 2 лабораторной получить навыки разработки API.
## Авторы
<img src="images/ava.png" width="200"> Владимиров Александр 
## Полезные ссылки
[Самоучитель по питону](https://pythontutor.ru/)  
  
